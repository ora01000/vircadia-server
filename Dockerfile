FROM agdxbas01v.agdx-paas.lguplus.co.kr.novalocal:5000/library/ubuntu:18.04

ARG LD_LIBRARY_PATH=/opt/vircadia/lib

RUN apt-get update -y && \
    apt-get install -y \
    libglib2.0-0 \
    libsm6 \
    libxext6 \
    libxrender-dev \
    libgl1-mesa-dev \
    git \
    vim \
    net-tools \
    wget \
    # cleanup
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/li

RUN wget https://gitlab.com/ora01000/vircadia-server/-/raw/main/vircadia-server-fixed.deb?inline=false -O tt.deb && \ 
    apt-get install -y ./tt.deb && \
    rm ./tt.deb

RUN /opt/vircadia/new-server lguplus 40100
CMD /opt/vircadia/domain-server lguplus


